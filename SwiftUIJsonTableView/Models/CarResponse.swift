//
//  CarResponse.swift
//  SwiftUIJsonTableView
//
//  Created by Viktoriia Skvarko on 14.08.2021.
//

import Foundation
import UIKit
import SwiftUI

struct CarResponse: Hashable, Codable, Identifiable {
    var id: Int
    
    var imageName: String
    var brendAndModel: String
    var description: String
}
