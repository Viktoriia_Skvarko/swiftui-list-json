//
//  ContentView.swift
//  SwiftUIJsonTableView
//
//  Created by Viktoriia Skvarko on 14.08.2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            List(carResponse) { car in
                CellCarsList(car: car)
            }
            .navigationBarTitle(Text("Cars"))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
