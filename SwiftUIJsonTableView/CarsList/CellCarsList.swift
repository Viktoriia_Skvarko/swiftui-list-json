//
//  CellCarsList.swift
//  SwiftUIJsonTableView
//
//  Created by Viktoriia Skvarko on 14.08.2021.
//

import SwiftUI

struct CellCarsList: View {
    
    var car: CarResponse
    
    var body: some View {
        VStack() {
            HStack() {
                Image(car.imageName)
                    .resizable()
                    .frame(width: 80, height: 80)
                    .clipShape(Circle())
                Text(car.brendAndModel)
                Spacer()
            }.padding()
            HStack() {
                Text(car.description)
                    .lineLimit(nil)
            }.padding()
        }
    }
}

struct CellCarsList_Previews: PreviewProvider {
    static var previews: some View {
        CellCarsList(car: carResponse[0])
    }
}
